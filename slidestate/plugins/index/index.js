var index = (function() {
	var active = false;
	var slides = '';
	var created = false;

	function createIndex() {
		$('body > *').css('display:none;');

		$('body').append('<div id="index-container"></div>');
		var visibleNow = $('section.visible');
		$('section').addClass('visible');
		
		var numSlides = $('section').length;
		function countdown() {
			if((--numSlides) == 0) {
				$('section').removeClass('visible');
				visibleNow.addClass('visible');
				window.requestAnimationFrame(function() {
					$('.slide-thumbnail').addClass('visible');
				});
			}
		} 
		var w = $(window).width()*0.23;
		var h = $(window).height()*0.23;
		$('section').each(function(index, element) {
			html2canvas(element, {
				onrendered: function(canvas) {
					var extra_canvas = document.createElement('canvas');
					extra_canvas.setAttribute('width',w);
					extra_canvas.setAttribute('height',h);
					var ctx = extra_canvas.getContext('2d');
					ctx.drawImage(canvas,0,0,canvas.width, canvas.height,0,0,w,h);
					// insert the thumbnail at the top of the page
					$('body #index-container').append('<div class="slide-thumbnail"></div>');
					$('body #index-container .slide-thumbnail').last().css('width', w);
					$('body #index-container .slide-thumbnail').last().css('height', h);
					$('body #index-container .slide-thumbnail').last().append(extra_canvas);
					$('body #index-container .slide-thumbnail').last().append($('<span class="sl-th-index">'+element.id+' ['+(index+1)+']</span>'));
					$('body #index-container .slide-thumbnail').last().click(function() {
						$('.slide-thumbnail').removeClass('visible');
						$('#index-container').css('display', 'none');		
						active = !active;
						slidestate.navigate.to(element.id);
					});
					countdown();
				}
			});
		});
	}

	function restore() {
		$('.slide-thumbnail').removeClass('visible');
		$('.slide-thumbnail').first().one('webkitTransitionEnd', function() {
			$('#index-container').css('display', 'none');
		});
	}

	function display() {
		$('#index-container').css('display', 'block');
		setTimeout(function () {
			$('.slide-thumbnail').addClass('visible');
		},100);
	}

	return {
		init: function() {
			slidestate.onKey(73, function() {
				active = !active;
				if(!created) {
					createIndex();
					created = true;
				} else if(active) {
					display();
				} else {
					restore();
				}
			})
		},
		update: function () {}
	}
})();
