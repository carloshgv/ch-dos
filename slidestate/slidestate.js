/*
 *
 */
console.log('starting');
var slidestate = (function($) {
	/* private */
	var slides = [];
	var nameToIndex = {};
	var current = 0;
	var last = 0;
	var controllers = {};
	var plugins = [];
	var pluginConfig = {};

	var keyController = {
		33: function() {
			prev();
		},
		37: function() {
			prev();
		},
		38: function() {
			prev();
		},
		34: function() {
			next();
		},
		39: function() {
			next();
		},
		40: function() {
			next();
		}
	};

	function forward(from, to) {
		in_transition = 2;
		from.addClass('leave');
		from.on('webkitAnimationEnd', function(e) {
			if(e.originalEvent.animationName=='leave') {
				from.removeClass('leave');
				from.removeClass('visible');
				in_transition--;
				from.off('webkitAnimationEnd');
			}
		});
		to.addClass('visible');
		to.addClass('enter');
		to.on('webkitAnimationEnd', function(e) {
			if(e.originalEvent.animationName=='enter') {
				to.removeClass('enter');
				in_transition--;
				to.off('webkitAnimationEnd');
			}
		});
	};

	function reverse(from, to) {
		in_transition = 2;
		from.css('-webkit-animation-direction', 'reverse');
		from.addClass('enter');
		from.on('webkitAnimationEnd', function(e) {
			if(e.originalEvent.animationName=='enter') {
				from.removeClass('enter');
				from.removeClass('visible');
				from.css('-webkit-animation-direction', '');
				in_transition--;
				from.off('webkitAnimationEnd');
			}
		});
		to.addClass('visible');
		to.css('-webkit-animation-direction', 'reverse');
		to.addClass('leave');
		to.on('webkitAnimationEnd', function(e) {
			if(e.originalEvent.animationName=='leave') {
				to.removeClass('leave');
				to.css('-webkit-animation-direction', '');
				in_transition--;
				to.off('webkitAnimationEnd');
			}
		});
	};

	function next() {
		var current_step = slides[current].data('current_step');
		if(current_step < slides[current].data('steps')) {
			slides[current].data('current_step', ++current_step);
			if((current_step>1)&&(((controllers[slides[current].attr('id')]||{}).steps||{})[current_step-1]||{}).out)
				controllers[slides[current].attr('id')].steps[current_step-1].out(
					slides[current].find('[step='+(current_step-1)+']')
				);
			if((((controllers[slides[current].attr('id')]||{}).steps||{})[current_step]||{}).in)
				controllers[slides[current].attr('id')].steps[current_step].in(
					slides[current].find('[step='+current_step+']')
				);
			else
				slides[current].find('[step]').filter(function() {
					return $(this).attr('step') <= current_step; }).addClass('visible');
		}
		else if(current < last) {
			slides[current].data('current_step', current_step);
			window.location.hash = slides[current+1].attr('id');
		}
	};

	function prev() {
		var current_step = slides[current].data('current_step');
		if(current_step > 0) {
			slides[current].data('current_step', --current_step);
			if((current_step<slides[current].data('steps'))&&
			  (((controllers[slides[current].attr('id')]||{}).steps||{})[current_step+1]||{}).out)
				controllers[slides[current].attr('id')].steps[current_step+1].out(
					slides[current].find('[step='+(current_step+1)+']')
				);
			else
				slides[current].find('[step]').filter(function() {
					return $(this).attr('step') > current_step; }).removeClass('visible');

			if((((controllers[slides[current].attr('id')]||{}).steps||{})[current_step]||{}).in)
				controllers[slides[current].attr('id')].steps[current_step].in(
					slides[current].find('[step='+current_step+']')
				);
		}
		else if(current > 0) {
			slides[current].data('current_step', current_step);
			window.location.hash = slides[current-1].attr('id');
		}
	};

	function keyListener(event) {
		if(in_transition) return;
		if(keyController[event.which]) keyController[event.which]();
	};

	/* public */
	function go(to) {
		var from = slides[current].attr('id');
		var toIndex = nameToIndex[to];
		if(toIndex === undefined) {
			window.alert('wrong slide');
			return;
		}

		if(toIndex < current) {
			reverse(slides[current], slides[toIndex]);
			current = toIndex;
		}
		else if(toIndex > current) {
			forward(slides[current], slides[toIndex]);
			current = toIndex;
		}
		
		if((controllers[from]||{}).leave)
			controllers[from].leave(slides[current]);
		if((controllers[to]||{}).enter)
			controllers[to].enter(slides[nameToIndex[to]]);

		plugins.forEach(function (p) { p.update(); });
	};

	function init(root) {
		$('#' + root + ' section').each(function(index) {
			nameToIndex[$(this).attr('id')] = index;
			slides[index] = $(this);
			var maximum = null;
			$(this).find('[step]').each(function() {
				var value = parseFloat($(this).attr('step'));
				maximum = (value > maximum) ? value : maximum;
			});
			$(this).data('steps', maximum || 0);
			$(this).data('current_step', 0);
		});
		last = slides.length-1;
		in_transition = 0;
		$(document).keydown(keyListener);
		$(window).on('popstate', function(e) {
			console.log(window.location.hash.slice(1));
			go(window.location.hash.slice(1)||slides[0].attr('id'));
		});
	};

	function controller(slide, ctrl) {
		controllers[slide] = ctrl(slides[nameToIndex[slide]]);
	};

	function plugin(plugin, config) {
		plugins.push(plugin);
		pluginConfig[plugin.name] = config;
	}

	function start(defaultSlide) {
		current = nameToIndex[window.location.hash.slice(1) || defaultSlide];
		slides[current].addClass('visible');
		if((controllers[slides[current].attr('id')]||{}).enter)
			controllers[slides[current].attr('id')].enter(slides[current]);

		plugins.forEach(function(p) { p.init(pluginConfig[p.name]); });
	};

	function onKey(key, listener) {
		keyController[key] = listener;
	}

	return {
		navigate : {
			to: function(dst) { window.location.hash = dst; },
			next: next,
			prev: prev
		},
		slides : {
			total: function() { return slides.length; },
			get: function() { return slides; },
			position: function() { return current+1; }
		},
		progress: function() { return (current/(slides.length-1))*100; },
		init: init,
		controller: controller,
		plugin: plugin,
		onKey: onKey,
		start: start
	};
})(jQuery);
