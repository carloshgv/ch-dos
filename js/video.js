var video = (function() {
	var memory = new Array(30);

	function zeromem() {
		for(var i = 0; i < 30; i++)
			memory[i] = new Array(80);
	}

	var output = {};

	function draw(cursor) {
		buffer = '';
		for(var i = 0; i < memory.length; i++) {
			for(var j = 0; j < memory[i].length; j++) {
				if(memory[i][j] == ' ' || !memory[i][j])
					buffer += '&nbsp;';
				else if(memory[i][j] == '<')
					buffer += '&lt;';
				else if(memory[i][j] == '>')
					buffer += '&gt;';
				else
					buffer += (memory[i][j]);
			}
			buffer += '<br/>';
		}
		output.innerHTML = buffer;
		if(cursor && !cursor.off) {
			var indicator=document.createElement('SPAN');
			indicator.style.position = 'absolute';
			indicator.style.height = '0.2em';
			indicator.style.width = '0.6em';
			indicator.style.background = '#00FF00';
			indicator.style.left = (cursor.x+2)*9-3;
			indicator.style.top = (cursor.y+2)*16-4;
			indicator.style.visibility = 'visible';
			output.appendChild(indicator);
			function toggle() {
				if(indicator.style.visibility == 'visible')
					indicator.style.visibility = 'hidden';
				else
					indicator.style.visibility = 'visible';
				setTimeout(toggle, 200);
			}
			toggle();
		}
	}

	function scroll(amount) {
		memory = memory.slice(amount);
		for(var i = 0; i < amount; i++)
			memory.push(new Array(80));
	}

	return {
		memory: function() { return memory; },
		plug: function(o) { output = o; zeromem(); draw(); },
		zeromem: zeromem,
		draw: draw,
		scroll: scroll
	};
})();
