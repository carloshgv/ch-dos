programs.imgview = function(args) {
	var file = dos.openfile(dos.fullpath(args[0]));
	if(!file) {
		bios.writeln('file not found');
		dos.return();
		return;
	}
	if(file.tagName != 'A' || file.className.indexOf('image') == -1) {
		bios.writeln('wrong file type');
		dos.return();
		return;
	}

	var imgDiv = document.createElement('DIV');
	imgDiv.id = 'imgviewer';
	var img = document.createElement('IMG');
	img.src = file.href;
	imgDiv.appendChild(img);
	var container = document.querySelector('.container');
	container.appendChild(imgDiv);
	setTimeout(function() {
		imgDiv.style.opacity = 1;
	}, 100);

	function closeViewer(ev) {
		container.removeChild(imgDiv);
		ev.currentTarget.removeEventListener('click', closeViewer);
		document.removeEventListener('keypress', closeViewer);
		dos.return(true);
	}
	setTimeout(function() {
		img.addEventListener('click', closeViewer);
		document.addEventListener('keypress', closeViewer);
	}, 100);
}
