var keyboard = (function() {
	var buffer = [];
	var keys = [];
	var input = {};

	function onkeypress(event) {
		switch(event.which) {
			case 13:
				break;
			default:
				buffer.push(String.fromCharCode(event.which));
		}
		bios.irq('keyboard', event.which);
	}

	function onkeydown(event) {
		switch(event.which) {
			case 8:
				event.preventDefault();
				buffer.pop();
				bios.irq('keyboard', event.which);
				break;
			default:
				keys.push(event.which);
		}
	}

	function flush() {
		buffer = [];
		keys = [];
	}

	function poll() {
		return keys.shift();
	}

	function plug(inp) {
		input = inp;
		input.addEventListener('keydown', onkeydown);
		input.addEventListener('keypress', onkeypress);
	}

	return {
		flush: flush,
		buffer: function() { return buffer; },
		poll: poll,
		plug: plug
	}
})();
