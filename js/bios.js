var bios = (function() {
	var cursor = { x: 0, y: 0, readPos: 0 };
	var callback = {};

	var irq_enabled = {};

	var irq_handlers = {
		'keyboard': function(key) {
			switch(key) {
				case 13: 
					cursorDown();
					if(callback['readln']) {
						irq_enabled['keyboard'] = false;
						callback['readln'](keyboard.buffer().join(''));
					}
					break;
				case 8:
					if(cursor.x > cursor.readPos) {
						cursor.x--;
						video.memory()[cursor.y][cursor.x] = ' ';
					}
					break;
				default:
					write(String.fromCharCode(key));
			}
			video.draw(cursor);
		}
	}

	function start(config) {
		video.plug(config.monitor);
		keyboard.plug(config.keyboard);
		config.keyboard.focus();
		writeln('CH-DOS v1.0');
		writeln('320kb memory');
		writeln('System check OK');
		writeln('==');
		writeln('Enter "type about.txt" for details');
		cursorDown();
		command();
	}

	function irq(channel, data) {
		if(irq_enabled[channel])
			irq_handlers[channel](data);
	}

	function cursorDown() {
		cursor.x = 0;
		cursor.readPos = 0;
		cursor.y++;
		if(cursor.y >= video.memory().length) {
			cursor.y = video.memory().length - 1;
			video.scroll(1);
		}
	}

	function write(string, newline, dontredraw) {
		for(var i = 0; i < string.length; i++) {
			if(cursor.x >= video.memory()[cursor.y].length || string[i] == '\n') {
				cursorDown();
			}
			video.memory()[cursor.y][cursor.x++] = string[i];
		}
		if(newline)
			cursorDown();
		if(!dontredraw)
			video.draw(cursor);
	}

	function writeln(string) {
		write(string, true, false);
	}

	function readln(cb) {
		irq_enabled['keyboard'] = true;
		keyboard.flush();
		cursor.readPos = cursor.x;
		callback['readln'] = cb;
	}

	return {
		start: start,
		write: write,
		writeln: writeln,
		readln: readln,
		cursorOff: function() {
			cursor.off = true;
		},
		cursorOn: function() {
			cursor.off = false;
		},
		cursorTo: function(x, y) {
			cursor.x = x;
			cursor.y = y;
		},
		cursorDown: cursorDown,
		cursorBegin: function() { cursor.x = 0; },
		clrscr: function() { 
			video.zeromem();
			cursor.x = 0;
			cursor.y = 0;
			cursor.readPos = 0;
		},
		scrsize: function() { return { h: video.memory().length, w: video.memory()[0].length }; },
		irq: irq,
	};
})();
