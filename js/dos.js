var dos = (function() {
	function navigateTo(path) {
		var dir = document.getElementById(path[0].toUpperCase());
		for(var i = 1; i < path.length; i++) {
			if(!dir) {
				return null;
			}
			if(path[i] == '.')
				dir = dir;
			else if(path[i] == '..')
				dir = dir.parentNode;
			else {
				for(var j = 0; j < dir.children.length; j++) {
					var found = false;
					if(dir.children[j].id == path[i].toUpperCase()) {
						found = true;
						dir = dir.children[j];
						break;
					}
				}
				if(!found) {
					return null;
				}
			}
		}

		return dir;		
	}

	function opendir(path) {
		var dir = navigateTo(path);
		if(!dir) return null
		return dir.children;
	}

	function openfile(path) {
		var node = navigateTo(path);
		if(!node) return null
		if(node.className.indexOf('file') == -1)
			return null;
		else
			return node;
	}

	function fullpath(path) {
		var relative;
		if(path[1] == ':')
			relative = path.toUpperCase().split('\\');
		else if(path[0] == '\\')
			relative = [drive].concat(path.toUpperCase().split('\\'));
		else
			relative = [drive].concat(currentPath).concat(path.toUpperCase().split('\\'));

		relative = relative.filter(function(a) { return a });
		var absolute = relative.splice(0, 1);
		relative.forEach(function(dir) {
			if(dir == '..' && absolute.length > 1)
				absolute.pop();
			else if(dir == '..' && absolute.length == 1)
				return;
			else if(dir == '.')
				return;
			else
				absolute.push(dir);
		});
		return absolute;
	}

	return {
		opendir: opendir,
		openfile: openfile,
		fullpath: fullpath,
		return: function(comm) { 
			delete dos.running;
			if(comm)
				command();
		}
	}
})();

var programs = {};