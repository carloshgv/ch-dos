var drive = 'C:';
var currentPath = [ ];

var command = function() {
	function process(input) {
		if(input.length > 0) {
			var commandLine = input.split(' ');
			if(commandLine[0][1] == ':' && commandLine[0].length == 2) {
				var driveRoot = document.getElementById(commandLine[0].toUpperCase());
				if(driveRoot == null)
					bios.writeln('drive not found');
				else
					drive = commandLine[0].toUpperCase();
			}
			else if(dos.openfile(dos.fullpath(commandLine[0].toUpperCase()+(commandLine[0].toUpperCase().indexOf('.EXE') == -1 ? '.EXE' : '')))) {
				dos.running = commandLine[0].toLowerCase();
				var exe = commandLine[0].toLowerCase().split('\\');
				exe = exe[exe.length - 1].replace('.exe', '');
				programs[exe](commandLine.splice(1));
			}
			else if(dos.openfile(['C:', 'CHDOS'].concat([commandLine[0].toUpperCase()+(commandLine[0].toUpperCase().indexOf('.EXE') == -1 ? '.EXE' : '')]))) {
				dos.running = commandLine[0].toLowerCase().replace('.exe', '');
				programs[dos.running](commandLine.splice(1));
			}
			else if(internals[commandLine[0].toLowerCase()])
				internals[commandLine[0].toLowerCase()](commandLine.splice(1));
			else
				bios.writeln('command not found');
		}
		if(!dos.running)
			prompt();
	}

	function prompt() {
		bios.write(drive + '\\' + currentPath.join('\\') + '\>');
		bios.readln(process);
	}

	prompt();
};

var internals = {};

internals.echo = function(args) {
	bios.writeln(args.join(' '));	
};

internals.type = function(args) {
	var file = dos.openfile(dos.fullpath(args[0]));
	if(!file) {
		bios.writeln('file not found');
		return;
	}
	if(args[0].toUpperCase() === 'ABOUT.TXT' &&
		document.querySelector('.second-panel').style.visibility !== 'visible') {
		document.querySelector('.second-panel').style.visibility = 'visible';
		document.querySelector('.menu').style.visibility = 'visible';
		setTimeout(function() {
			document.querySelector('.main-panel').style.top = '-29em';
			document.querySelector('.second-panel').style.top = '4em';
		}, 2000);
		function restore() {
			document.querySelector('.main-panel').style.top = '0';
			document.querySelector('.second-panel').style.top = '33em';		
			document.removeEventListener('keydown', restore);	
		}
		document.addEventListener('keydown', restore);
	}
	for(var i = 0; i < file.textContent.length; i++) {
		if(file.textContent[i]==String.fromCharCode(10))
			bios.cursorDown();
		else
			bios.write(file.textContent[i]);
	}
	bios.writeln('');
};

internals.cls = function() {
	bios.clrscr();
};

internals.eval = function(args) {
	try {
		bios.writeln(String.call('toString', eval(args.join(' ')) || ''));
	} catch(err) {
		bios.writeln(err.message);
	}
}

internals.dir = function(args) {
	if(args[0]) {
		bios.writeln('Directory of ' + dos.fullpath(args[0]).join('\\'));
		var entries = dos.opendir(dos.fullpath(args[0]));
	}
	else {
		bios.writeln('Directory of ' + [drive].concat(currentPath).join('\\'));
		var entries = dos.opendir([drive].concat(currentPath));
	}
	if(!entries) {
		bios.writeln('directory not found');
		return;
	}
	var files = 0, dirs = 0;
	for(var i = 0; i < entries.length; i++) {
		var line = sprintf('%-10s %-4s %-6s',
			entries[i].id.split('.')[0],
			entries[i].id.split('.')[1]||'',
			entries[i].className.indexOf('directory') != -1 ? '<DIR>':'');
		bios.writeln(line);
		entries[i].className.indexOf('directory') != -1 ? dirs++ : files++;
	}
	bios.writeln(sprintf('%6d File(s)', files));
	bios.writeln(sprintf('%6d Dir(s)', dirs));
};

internals.cd = function(args) {
	if(args.length == 0) {
		bios.writeln('usage: cd <path>');
		return;
	}

	var destination = dos.fullpath(args[0].toUpperCase());
	if(!dos.opendir(destination)) {
		bios.writeln('directory not found');
		return;
	}
	drive = destination.splice(0, 1)[0];
	currentPath = destination;
};

internals.get = function(args) {
	if(args.length == 0) {
		bios.writeln('usage: get <file>');
		return;
	}
	var file = dos.openfile(dos.fullpath(args[0]));
	if(!file) {
		bios.writeln('file not found');
		return;
	}
	if(file.tagName != 'A') {
		bios.writeln('permission denied');
		return;
	}
	if(file.className.indexOf('bigfile') != -1) {
		file.target = '_blank';
		file.click();
		file.removeAttribute('target');
	} else {
		file.setAttribute('download', '');
		file.click();
		file.removeAttribute('download');
	}
}

internals.antes = function() {
	window.location = 'antes/';
}

