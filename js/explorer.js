programs.newTab = function(prog, args, type) {
	if(!args[0]) {
		bios.writeln('use: ' + prog + ' <file>')
		return;
	}
	var file = dos.openfile(dos.fullpath(args[0]));
	if(!file) {
		bios.writeln('file not found');
		return;
	}
	if(file.tagName != 'A' || file.className.indexOf(type) == -1) {
		bios.writeln('wrong file type');
		return;
	}

	file.target = '_blank';
	file.click();
	file.removeAttribute('target');
}

programs.explorer = function(args) {
	programs.newTab('explorer', args, 'web');
	dos.return();	
}

programs.pdfread = function(args) {
	programs.newTab('pdfread', args, 'pdf');
	dos.return();
}