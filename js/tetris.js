programs.tetris = function() {
	var stbasil = '\
                            . \n\
                            T \n\
                           ( ) \n\
                          <===> \n\
                           F|J \n\
                           === \n\
                          J|||F \n\
                          F|||J \n\
                         /\\/ \\/\\ \n\
                         F+++++J \n\
                        J{}{|}{}F         . \n\
                     .  F{}{|}{}J         T \n\
          .          T J{}{}|{}{}F        ;; \n\
          T         /|\\F/\\/\\|/\\/\\J  .   ,;;;;. \n\
         /:\\      .\'/|\\\\:=========F T ./;;;;;;\\ \n\
       ./:/:/.   ///|||\\\\\\"""""""" /x\\T\\;;;;;;/ \n\
      //:/:/:/\\  \\\\\\\\|////..[ ]...xXXXx.|====| \n\
      \\:/:/:/:T7 :.:.:.:.:||[ ]|/xXXXXXx\\||||| \n\
      ::.:.:.:A. `;:;:;:;\'=== ==\XXXXXXX/=====. \n\
      `;""::/xxx\\.|,|,|,| ( ) ( )| | | |.=..=.| \n\
       :. :`\\xxx/(_)(_)(_) _   _ | | | |\'-\'\'-\'| \n\
       :T-\'-.:"":|"""""""|/ \\ / \\|=====|======| \n\
       .A."""||_|| ,. .. || | | |/\\/\\/\\/ | | || \n\
   :;:////\\:::.\'.| || || ||-| |-|/\\/\\/\\+|+| | | \n\
  ;:;;\\////::::,=\'=======\'=============/\\/\\=====. \n\
 :;:::;""":::::;:|__..,__|============/||\\|\\====| \n\
 :::::;|=:::;:;::|,;:::::          |========|   | \n\
 ::l42::::::(}:::::;::::::_________|========|___|__ \
';

	var timer = Date.now(),
	    interval = 600,
	    score = 0,
	    lines = 0,
	    linesThisLevel = 0,
	    level = 0;

	function sleep(milliseconds) {
		var start = new Date().getTime();
		for (var i = 0; i < 1e7; i++) {
			if ((new Date().getTime() - start) > milliseconds) {
				break;
			}
		}
	}

	function drawGrid() {
		var mem = video.memory();

		for(var i = 0; i < grid.h-2; i++) {
			mem[grid.y+i][grid.x] = '│';
			for(var j = 0; j < grid.w*2; j+=2) {
				mem[grid.y+i][grid.x+j+1] = grid.data[i+2][j/2] ? '█' : '·';
				mem[grid.y+i][grid.x+j+2] = grid.data[i+2][j/2] ? '█' : '·';
			}
			mem[grid.y+i][grid.x+j+1] = '│';
		}
		mem[grid.y+grid.h-2][grid.x] = '└';
		for(var j = 0; j < grid.w*2; j+=2) {
			mem[grid.y+grid.h-2][grid.x + j + 1] = '─';
			mem[grid.y+grid.h-2][grid.x + j + 2] = '─';
		}
		mem[grid.y+grid.h-2][grid.x+grid.w*2+1] = '┘';
	}

	function collision(x, y, form) {
		if(y + form.length > grid.h || x < 0 || x + form[0].length > grid.w)
			return true;
		for(var i = 0; i < form.length; i++)
			for(var j = 0; j < form[0].length; j++)
				if(grid.data[y+i][x+j] && form[i][j])
					return true;
		return false;
	}

	function drawPiece(x, y, next) {
		if(next) {
			var mem = video.memory();
			for(var i = 0; i < next.length; i++)
				for(var j = 0; j < next[0].length*2; j+=2) {
					mem[y+i][x+j  ] = next[i][j/2] ? '█' : ' ';
					mem[y+i][x+j+1] = next[i][j/2] ? '█' : ' ';
				}
		} else {
			for(var i = 0; i < piece.form[piece.rot].length; i++)
				for(var j = 0; j < piece.form[piece.rot][0].length; j++)
					grid.data[piece.y+i][piece.x+j] ^= piece.form[piece.rot][i][j];
		}
	}

	function cleanGrid() {
		var numRows = 0;
		for(var i = grid.h-1; i > 0; i--) {
			var fullRow = true;
			for(var j = 0; j < grid.w; j++)
				fullRow = fullRow && grid.data[i][j];
			if(fullRow) {
				numRows++;
				for(var ii = i; ii > 1; ii--)
					for(var j = 0; j < grid.w; j++)
						grid.data[ii][j] = grid.data[ii-1][j];
				i++;
			}
		}
		return numRows;
	}

	function drawBox(x, y, w, h) {
		bios.cursorTo(x, y);
		bios.write('╔', false, true);
		bios.write(new Array(w-2).join('═'), false, true);
		bios.write('╗', false, true);
		for(var i = 1; i < h-1; i++) {
			bios.cursorTo(x, y+i);
			bios.write('║', false, true);
			bios.cursorTo(x+w-2, y+i);
			bios.write('║', false, true);			
		}
		bios.cursorTo(x, y+h-1);
		bios.write('╚', false, true);
		bios.write(new Array(w-2).join('═'), false, true);
		bios.write('╝', false, true);		
	}

	function drawGUI() {
		bios.cursorTo(40, 5);
		bios.write('next', false, true);
		drawBox(40, 6, 9, 6);
		drawPiece(42, 7, next.form[0]);

		bios.cursorTo(40, 15);
		bios.write('score', false, true);
		bios.cursorTo(40, 16);
		bios.write(score.toString(), false, true);

		bios.cursorTo(40, 18);
		bios.write('lines', false, true);
		bios.cursorTo(40, 19);
		bios.write(lines.toString(), false, true);

		bios.cursorTo(40, 21);
		bios.write('level', false, true);
		bios.cursorTo(40, 22);
		bios.write(level.toString(), false, true);
	}

	function levelUp() {
		level++;
		if(level <= 13)
			interval = Math.floor(600.0 * (1.18 - Math.log10(level+0.5)));
	}

	var dirty = false;
	var slide = 0;
	function update() {
		if(dirty) drawPiece();
		dirty = true;

		var key = keyboard.poll();
		switch(key) {
			case 37:
				piece.x--;
				if(collision(piece.x, piece.y, piece.form[piece.rot]))
					piece.x++;
				slide=0;
				break;
			case 39:
				piece.x++;
				if(collision(piece.x, piece.y, piece.form[piece.rot]))
					piece.x--;
				slide=0;
				break;
			case 38:
			case 32:
				piece.rot = (piece.rot+1)%piece.form.length;
				if(piece.x + piece.form[piece.rot][0].length > grid.w)
					piece.x = grid.w - piece.form[piece.rot][0].length;
				while(collision(piece.x, piece.y, piece.form[piece.rot]))
					piece.y--;
				slide=0;
				break;
			case 40:
			    for(var i = piece.y; i < grid.h; i++)
			    	if(collision(piece.x, i, piece.form[piece.rot]))
			    		break;
				piece.y = i-1;
				timer = Date.now();
				break;
			case 187:
				levelUp();
				break;
			case 27:
				return true;
		}
		if(Date.now() - timer > interval) {
			piece.y++;
			if(collision(piece.x, piece.y, piece.form[piece.rot])) {
				piece.y--;
				slide++;
			}
			timer = Date.now();
		}

		if(slide > 1) {
			drawPiece();
			if(piece.y < 1)
				return true;
			piece = next; 
			next = new Piece();
			dirty = false;
			slide = 0;
			var cleared = cleanGrid();
			lines += cleared;
			linesThisLevel += cleared;
			score += cleared*cleared*10;
			if(linesThisLevel >= 10) {
				linesThisLevel -= 10;
				levelUp();
			}
			return;
		}

		drawPiece();
	}

	function Piece() {
		this.x = 4;
		this.y = 0;
		this.rot = 0;
		var np = Math.floor(Math.random()*pieces.length);
		this.form = pieces[np];
	}

	var pieces = [
		[
			[
				[1,0],
				[1,1],
				[1,0]
			],
			[
				[1,1,1],
				[0,1,0]
			],
			[
				[0,1],
				[1,1],
				[0,1]
			],
			[
				[0,1,0],
				[1,1,1]
			]
		],
		[
			[
				[0,1],
				[1,1],
				[1,0]
			],
			[
				[1,1,0],
				[0,1,1]
			]
		],
		[
			[
				[1,0],
				[1,1],
				[0,1]
			],
			[
				[0,1,1],
				[1,1,0]
			]
		],
		[
			[
				[1,0],
				[1,0],
				[1,1]
			],
			[
				[1,1,1],
				[1,0,0]
			],
			[
				[1,1],
				[0,1],
				[0,1]
			],
			[
				[0,0,1],
				[1,1,1]
			]
		],
		[
			[
				[0,1],
				[0,1],
				[1,1]
			],
			[
				[1,0,0],
				[1,1,1]
			],
			[
				[1,1],
				[1,0],
				[1,0]
			],
			[
				[1,1,1],
				[0,0,1]
			]
		],
		[
			[
				[1],
				[1],
				[1],
				[1]
			],
			[
				[1,1,1,1]
			]
		],
		[
			[
				[1,1],
				[1,1]
			]
		]
	];

	function mainLoop() {
		if(update()) {
			sleep(1000);
			bios.cursorOn();
			bios.clrscr();
			bios.writeln(stbasil);
			bios.writeln(sprintf('  ---[ score: %11d ]---[ lines: %6d ]---', score, lines, level));
			dos.return(true);
			return;	
		}
		bios.clrscr();
		drawGrid();
		drawGUI();
		video.draw();
		requestAnimationFrame(mainLoop);
	}

	/***************************/
	var grid = {
		x: 15,
		y: 4,
		w: 10,
		h: 22,
	}
	grid.data = new Array(grid.h);
	for(var i = 0; i < grid.h; i++)
		grid.data[i] = new Array(grid.w);

	var piece = new Piece();
	var next = new Piece();
	levelUp();

	bios.cursorOff();
	keyboard.flush();
	mainLoop();
}
