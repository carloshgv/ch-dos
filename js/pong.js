programs.pong = function() {
	var player = [
		{
			y : bios.scrsize().h/2 - 3,
			score : 0
		},
		{
			y : bios.scrsize().h/2 - 3,
			score : 0
		}
	];

	var ball = { x: 15, y: 15, deltaX: 1, deltaY: 1 };

	function draw() {
		bios.clrscr();
		var center = bios.scrsize().w / 2;
		for(var i = 0; i < bios.scrsize().h; i++) {
			bios.cursorTo(center, i);
			bios.write(':');
		}
		for(var i = 0; i < 6; i++) {
			bios.cursorTo(2, player[0].y+i);
			bios.write('█');
			bios.cursorTo(77, player[1].y+i);
			bios.write('█');
		}
		bios.cursorTo(5, 29);
		bios.write(player[0].score.toString());
		bios.cursorTo(74, 29);
		bios.write(player[1].score.toString());

		bios.cursorTo(ball.x, ball.y);
		bios.write('*');
	}

	bios.cursorOff();
	keyboard.flush();

	function updateBall() {
		ball.x += ball.deltaX;
		ball.y += ball.deltaY;
		if(ball.y > 28) {
			ball.y = 28;
			ball.deltaY = -1;
		}
		if(ball.y < 0) {
			ball.y = 0;
			ball.deltaY = 1;
		}
		if(ball.x == 3 && ball.y >= player[0].y && ball.y <= player[0].y+6) {
			ball.deltaX = 1;
		}
		if(ball.x == 76 && ball.y >= player[1].y && ball.y <= player[1].y+6) {
			ball.deltaX = -1;
		}
		if(ball.x > 79) {
			player[0].score++;
			ball.x = 15;
			ball.y = 15;
			ball.deltaX = 1;
			ball.deltaY = 1;
		}
		if(ball.x < 0) {
			player[1].score++;
			ball.x = 64;
			ball.y = 15;
			ball.deltaX = -1;
			ball.deltaY = 1;
		}
	}

	function mainLoop() {
		var key = keyboard.poll();
		switch(key) {
			case 27:
				bios.cursorOn();
				bios.clrscr();
				bios.writeln('bye!');
				dos.return(true);
				return;
			case 65:
				player[0].y--;
				break;
			case 90:
				player[0].y++;
				break;
			case 38:
				player[1].y--;
				break;
			case 40:
				player[1].y++;
				break;
		}
		if(player[0].y < 0) player[0].y = 0;
		if(player[0].y > 22) player[0].y = 22;
		if(player[1].y < 0) player[1].y = 0;
		if(player[1].y > 22) player[1].y = 22;
		updateBall();
		draw();
		requestAnimationFrame(mainLoop);
	}
	mainLoop();
}
