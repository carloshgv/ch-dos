programs.mail = function() {
	bios.writeln('MAIL 1.0');
	bios.writeln('Send a message to contact@awebde.ch');
	bios.writeln('===================================');
	bios.writeln('Enter . to end the message');
	bios.writeln('');

	bios.write('name: ');
	bios.readln(function(name) {
		bios.write('email: ');
		bios.readln(function(from) {
			bios.write('subject: ');
			bios.readln(function(subject) {
				var text = [] ;
				function readlines() {			
					bios.readln(function(line) {
						if(line != '.') {
							text.push(line);
							readlines();
						} else {
							var email = {
								from_name: escape(name),
								from_email: escape(from),
								subject: escape(subject),
								text: escape(text.join('\n'))
							};
							var xtr = new XMLHttpRequest();
							xtr.open('POST', 'mail/mail.php', false);
							xtr.send(JSON.stringify(email));
							dos.return(true);
						}
					});
				}
				readlines();
			});
		});
	});
}
