var goblin = (function() {
	var template = '<video id="goblin" src="images/goblin.webm" autoplay loop style="display: none; z-index: -1000; position: fixed; left: calc(40% - 256px); bottom: 0vh"/>'
	var enabled = true;
	var start = 46, end = 48;

	return {
		init: function() {
			$('#pres').append(template);
		},
		update: function() {
			if(slidestate.slides.position() <= start || slidestate.slides.position() >= end)
				$('#goblin').css('display', 'none');
			else if(enabled)
				$('#goblin').css('display', 'initial');
			if(slidestate.slides.position() >= end - 1)
				enabled = false;
		}
	}
})();
