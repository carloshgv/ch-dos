#!/bin/bash

cat presentation.html | grep "<section" | sed 's/.*id=\"\([^\"]*\)\".*/\1/g' | awk '{ print NR, ":", $0 }'
